#!/usr/bin/env python3
import random
import textwrap

fillers = "+/1234567890"
spacechars = "+/"
b64chars = "+/0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

def b64only(string):
    'Destructive filter for b64 characters only.'
    return ''.join([x for x in string if x in b64chars])

def rand_join(string_list, joinchars):
    'Applies the "join" method of string objects, but with a random character from joinchars for each junction.'
    outstr = []
    outstr += string_list.pop(0)
    for bit in string_list:
        outp_bit = b64only(bit)
        if not outp_bit.strip(): continue
        outstr += random.choice(joinchars)
        outstr += outp_bit
    return ''.join(outstr)

def rand_case(string):
    'Applies either upper() or lower() randomly for each character in string.'
    return ''.join([getattr(x,random.choice(["upper","lower"]))() for x in string])

def pgpify(string, wrapwidth=70):
    'Wraps text, replaces whitespace with readable symbols, fills edges with numbers and symbols, and randomises case.'
    output_list = [ "-----BEGIN PGP MESSAGE-----",
                    "Version: FauxGPG v0.1",
                    ""]
    string_lines = textwrap.wrap(string, wrapwidth)
    for line in string_lines:
        line = line.strip()
        spare_charspace = wrapwidth - len(line)
        line_bits = line.split()
        joined_line = rand_join(line_bits, spacechars)
        rand_case_line = rand_case(joined_line)
        lfill = ''.join([random.choice(fillers) for x in range(0,int(spare_charspace/2))])
        rfill = ''.join([random.choice(fillers) for x in range(0,int((spare_charspace+1)/2))])
        output_list.append(lfill+rand_case_line+rfill)
    output_list.append("-----END PGP MESSAGE-----")
    return '\n'.join(output_list)

if __name__ == "__main__":
    import argparse, sys
    ArgP = argparse.ArgumentParser(
                description="A toy that outputs the contents of a text file (or stdin) as a fake GPG block.",
                epilog="by Cathal Garvey while badgering friends to care more about online privacy.")
    ArgP.add_argument("-i","--inputfile", type=argparse.FileType('r'), default=sys.stdin,
                help="File to open, default is stdin.")
    ArgP.add_argument("-o","--outputfile",type=argparse.FileType("w"), default=sys.stdout,
                help="File to write output to, default is stdout.")
    ArgP.add_argument("-w","--wrapwidth",type=int,default=70,
                help="Width to wrap and pad text lines to, default 70.")
    Args = ArgP.parse_args()
    with Args.inputfile as InStream:
        input_text = InStream.read()
    with Args.outputfile as OutStream:
        OutStream.write(pgpify(input_text, Args.wrapwidth)+"\n")
