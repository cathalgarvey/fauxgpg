FauxGPG
=======
by Cathal Garvey

What is this?
-------------
A toy for making phony PGP blocks from text input. FauxGPG wraps, cases and joins text in such a way that it superficially resembles a block of encrypted material of the format output by GPG or other implementations of PGP.

Of course, the eye is very good at recognising false-encrypted matter for what it is, so most people familiar with GPG/PGP will instantly notice that this isn't real, not least because the version header says "FauxGPG".

What is PGP, and Why FauxGPG?
-----------------------------
Email is one of the most insecure communications standards in common use.
Not only are emails stored on a server operated by people who are often unknown and untrusted to the user, they are often passed around between servers without encryption.

Not only are the technical implementation details of email worrying; also Email suffers remarkably low protections in Law; often, Police or paramilitary police-mimics can access email records without warrants or with minimal proof of reasonable suspicion. They may also access *all* email logs rather than a limited subset believed to be pertinent to the investigation they claim to be undertaking.

In contrast to snail-mail letters, which may only be intercepted with warrants granted under condition of probable cause, and may only be intercepted at time of delivery, email can be traced back potentially through years of highly personal archives.

Even clicking "Delete" in an email client or otherwise does not guarantee that email is securely deleted; that is entirely a matter of trust for the client.

All of the foregoing, though rarer at the time, was also a problem years ago when email first became commonplace, and thanks to the diligence and strong wills of some key members of the global Cypherpunk community, a scheme known as "Pretty Good Privacy" became a legally acceptable standard for enforcement of personal privacy. Using PGP, people could maintain a public "key" that anyone can use to send them private messages, but which cannot be used to decrypt those messages. A private key, maintained by the user alone, allows the user to read her or his messages. With PGP, suddenly the only parties to an email exchange were the sender and recipient.

However, PGP has several key shortcomings:
1) It's ugly as hell, cluttering emails with huge blocks of random symbols (although a properly configured mail client like Thunderbird with Enigmail neatly removes all this clutter from sight)
2) Confirming that a key is really owned by the recipient and not a malicious "man in the middle" requires a little bit of work, and has spawned a very confusing infrastructure of key exchange and peer-validation protocols that, for normal day-to-day exchanges, are unlikely to be necessary, but which put people off using PGP entirely.
3) Actually using PGP has traditionally been *hard*, but only because nerds who "got" PGP didn't bother making it trivial for others.
4) People don't give a toss about their human rights.

This may put the average PGP user in the position of having to badger certain technically capable and experienced friends into resuming use of PGP so that they can enjoy a degree of privacy and free speech online. That's why I wrote this script: essentially to say to certain people "I'm gonna make my emails to you so hellishly ugly, that you're gonna wish we were using PGP anyway". :)

Oh, and the name? "GPG" stands for "Gnu Privacy Guard", and is one of the most popular implementations of PGP, coming out of the box with most distributions of Linux these days and acting as the engine behind friendlier interfaces such as Thunderbird's Enigmail plugin. 

What is the state of email encryption?
--------------------------------------
Frankly, PGP is outdated insofar as it's "too late" to save email. There is no conceivable way that a decently large chunk of email users are suddenly going to wake up from a privacy-less stupor of Facebook addiction and google-spamming and realise that privacy is worth keeping.

Other efforts are underway to make private communication a "built-in" feature of future email platforms. One promising approach, although it's too early to consider it truly secure, is Bitmessage. I use this for fun, and if you want you can reach me at this address: BM-opSmZfNZHSzGDwdD5KzTnuKbzevSEDNXL

In the end, people will use what they want to use, and whether it's privacy-aware will probably be a matter of luck and not planning for most users. So, let's hope the next big thing in communication that kills email has fewer shortcomings.

I want to use this "PGP" Thing
------------------------------
Great, good initiative.
If you wanna contact me, here's my public key (see what I mean about ugly?):

-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.4.11 (GNU/Linux)

mQINBFES3ZMBEADjIBApyo6hm1jU2fdyCYm99nSnutjaVfwQ8egnLa6FLSyJyl96
2lGfdwb9e5isH/Tujt7VFhmU7carTVz7rtJbaCqGcgLY6IUEy4VGakZCjw6vsPUQ
NJZN7vS9wG9gUsTrw0QhtC8IQ+TAvrIERi32zab5amdw/znpa8p/tiLjEFapd0wE
Lyr9txlpPl3T+64eZ35SfneHF0OYPS79qpK3KJt3TDCYFrM41EevLVzQRosTnygk
8lMvQfqtNkdRJpmNHsjG1kFOzoEINVTyY4PeO6rRaeDp6H2KD5tu9SXsi8uF1Ayt
F0fB+1c/WHTEvtvNneGonOkWWJokzayfMRvoDhxGsrFsASvoLZ21m7U9Fdyid4Cw
iU8pAgRCq53CV+YGLv6qioPDpuba0S4cO/eUPR6APcKHcE0xU5A5yIKiCPVQQKG3
85P4BRC+GwZ2ni2/cz/jwzFi/j9CuSoU3L1fLGN5wHmmMvNOSyIgtCWx7tnxvmGE
eGbLtwfgv8pXFF9WoH1d2nvZNdgWNaGrp1zxssho62pnO9FTe6FgvIyLD4v7OyIP
kPoTjWaaZh/U0hXeQyQOXKeTFEAMblMmpDHeCYLwjtWZLo4Z8+0Ez5sk3B0OETH4
B0K7hwIhIBqp9Gcgq4haSpo+vfxRJH3ouRGfbNMCcras1s+yBmUbvR3mBQARAQAB
tI9DYXRoYWwgR2FydmV5IChPdGhlciBhY2NzOiBvbmV0cnVlY2F0aGFsQHR3aXR0
ZXIsIGNhdGhhbGdhcnZleUBnaXRodWIsIGNhdGhhbGdhcnZleUBnaXRvcmlvdXMs
IGluZGllYmlvdGVjaC5jb20pIDxjYXRoYWxnYXJ2ZXlAY2F0aGFsZ2FydmV5Lm1l
PokCSAQTAQgAMgIbAwULCQgHAwUVCgkICwUWAgMBAAIeAQIXgAUCUROgpBAYMTI3
LjAuMC4xOjk5OTk5AAoJEL0iNgSYi5CZgg0QAJmbWPKqkugJxOB3hYXpYVpkBWYE
0rrU4tmYMpB+DEUp8ZUP7nf9TOZGOBo102n35TRvwRayKHjspy0OxWgHBI5aVkBK
mDXkq1pHVRYyBn9FK74GM2a4B4c1Rs2BCi24BoBXJ8xcpK99Z9KoPTRZTb1EpA82
RsYVbb/6Cz1gzQtcbS7HPS10Gz9ZElmo62Z7S7eoEkfqgxr9XbasdWbrWuaxy1lW
bbPfdRUMV97RHjNyr3BmvrB89znaCEWYgdkfjcI0Tua63ffymtgE3qv1hSxL53GI
Yb/9WYxBPsKr259XK4ce1OUkGExNu91W46oIi/s8qvkel3ntDexT/i4v7K9l5wiN
qiMej/USGyCHuUoOfjrvYGtDm2Rt4Xoz+YNe/prquQkz1hWhFIuUzDUY8ketbPPt
MqvwRJ3Zx2biReN8O7Z5hzAG+W0TQxR6vjfIsZeEahW7rbLyK7k9gaVWn3pTgRVo
7oBiWWYp4T5cnFbtyi9sDkwkZaNU1ExLubj8nVdPaDpeFqWIrNHdlyeTPzoWQDsw
7kh/HKVvC+EWbLdiZZ52bfpxHpPJHwsaHH2k5hXxorESxS/gnumYYmfX+UpOSKzO
a3hmvPSTDxpbvIY8WwhCedfwU9n0idKB+Hzs//SZU2u1HFePTTvj+KuLBns4ja8P
kIJjFkZbqOQW5osPiQEcBBABCAAGBQJREt+1AAoJEGrLMO47PegIOQoH/2rqxrja
Xxl1s2U2X9GmvOZNJY24qx8vcbkAT8XLOYkEtnPBbSmGrZipD320RHDp1BWIwBDp
J6sz6eDaV087GnBAFzbmiDhDaRyukaIIx+/CJcSGqU11kNLPQpfCtKTPLfDiObMk
zHdu6PSAt0jkqGFgIOmMlk5Y+KVJdilIeCnnbPOjzkp5sjbSY0ao5MsLk5xyKicm
pfxkxXPXddQAzgK1Nj0SmTP1azl5J6ePaly1RZkNApF6uI/6nlrBoq4Ij4MrpVzW
xK8iTtvA4/7VOuTZ+EAsBC7Vhn1gcaD63bmELu70RGCQn49pkcSAB36zMrr3eeqb
i41Yk5JZg3iUkWOJAjcEEwEIACEFAlES3ZMCGwMFCwkIBwMFFQoJCAsFFgIDAQAC
HgECF4AACgkQvSI2BJiLkJnoUg//efW/Oqka179pVrWoQHuOKBB6SwDCUpcZuXYo
r4TJXf3DRujFFWz3D+Id12WtvA7C1oSXzXiCm2AUlZdwVvVEgloDZOU5iYDI7Zgt
xlMNVBzNt6SrYVrzikpYk6mQu5F0shLaXMjfaW2SxHv9i2KhRkXdKzuQ+yu0FAz4
kZRfJXZ2CEocJlIWCru7pbHwHzvB64bNmVUikESoLa5ou5ulKnd0gNB9aQFVwV9T
it1pUQXJUfY7PvND5QfkJRFaldb+Gy64lPEjrwzWs1irwv3kiQXIEes3tTICWIut
VcJ8AM9J4ietIOZfHq1wdOIQBDopY8Kz9H/+RLzNWosYDnj0UbWnw2mV4PdMB9fF
P3TIIZsbjJ4s+Qm9HdBrsP5iJbjRUX0EzEwNsIvaCPGtBPsVgNtOHMp5ccMXcCBO
WG4ZwPjUDgNyFQ22rNWLJYp1ZGYcxa0o7xSEXarDPde6q4OfhcrPkMIuC3jNfivA
8LcaNwzeZL6BArtaAKFb8zLeig9BBxaRhOLECuVbe0ZwMvsCgshV9HOGqnrchD3m
jaxIJ+Psr4FM0xU6dVBq/OMcGclRkO0bo+kCJ2qpCWpPAGyjiLYePxOU8eTK6ag/
ZGCxc03xMdMCS6OAMHY12Ap2J4sVdevn18Yb/Fok2P3NYwTK5Sv49iudR8AcRjr7
v+iQVEa5Ag0EURLdkwEQALkiPn0ZHpbqqULq7lk0zPnSOQ+WXxkuevZ055zaW5ST
z+u/ImG6Tmij3MVljMFR7mCG1dU+jkJEmpYyH2vcuxY3XBcDuXzaw0LLKAJ5LaDx
soys77Nd4UWChPZ9stxtluZgU/jlIHfazVHUV7i6NMLsE1SVDe1/mPtWVhSZDIbN
Z9KmTOljfdM3rGuQkgIg6IGRjGRuLwLBLE7/Gs/Bjb/6YM0eWdWsv3Yc0uitJ/11
zAWTB5INxRxS6JxswESniBGddug02ZsCYBOw9Fp1CFmwGFb1cdwQ79fDLsMGHhXx
Lj9fK7RzaMfeMBy+/SgSaBhO5rYiw1zYZ3xjkT+H805SyzNJO7QRjw2109VVzUFX
E3hjDsKAB4xN7x6ICukUv0qqsPwOX52P0hDdpxqmRPKjMTLFtY7KUFIgeG1+wvnn
PQCcFl+yE7Skg3XzagluJWNkm+0eS+gIGtEB/Fqz08/9lpl2cH76lxqO700WXK+U
DNgz223gnOiwGu8rhp1q32MKd11x1r4AyIG3m2BUa5yQ6/ZboTiw3+vQy5xcc4jh
I4/SIPvheRk5yrlJSIacZewSB7O0VeqzGjt5mln2gTgLtOsbNvmLMAYtul3tKNj7
TkE8SQOq3tB6wxtXePDnEychamF+F5w5NwCj+BAa7g1P6a7qq4j9FhpuWEJneCo3
ABEBAAGJAh8EGAEIAAkFAlES3ZMCGwwACgkQvSI2BJiLkJkNDg/9EyDfaP52xSOz
tYgW4mtQgoptUFjWMsDlVczK1YIyaoFiPtvcbrGEKCcZlcSNgFVS/MuZ/Rm4fqFU
cRW8JjoFUDTlYeMdDLap5u73jrZcuQSd+CUqUWXwpAxOPOSGmgsWxxRhjIn+h4oQ
2h1ziLwzyeKc9TBJs6qXISGeQe2SBwtOiRaRng/kPSXSePEf6gy88dL9JgPT8FY+
uR8CqRQr+MfOkoWa+6iGWkzHAnez/fNrb0K7fdIAeSVO1XhAwsjJ/5nipNshDoB7
8TuqvJYrqKi+872+Po0BGO+RkutbLi3w9A2QelzStFfSRzXcRX8nf9IdTYw+rdC6
LPxDCa7oOLIQ1aA2fR7ASXRQKBe31/glXNGs1fHPP+RGjeRo7AxN8cl6aIlk1pX3
+FadF5li3+rGqG3smg0SDl0qBe0I6cq2YRvNZWBu7figy8yNyW1Kae4JPCLrm4hk
nUVbRomPB8ayy6NdoCwQUOvW8RibK0rCLEfwXimy340Ugv6pwmaC/mXLwgVKmOX3
Brff12ebab/i/p5OzGGVBT1x0dK3380DN+pqyfwU3fFG26QFHMze9RUKWOqJzdOX
OuZ9SN8rRfrKyboZLY/DibxQNwe/RSQa98I3FOY97tQL2y/jOeEzegN5x7AicxGh
bLovIH3tu+pCFGeiHNEiWuTUt0YWI/I=
=q1bH
-----END PGP PUBLIC KEY BLOCK-----
